import java.util.Arrays;
import java.util.Collections;

public class Sheep {

   /** Test data */
   static Sheep.Animal[] animals = null;

   /** Number of goat animals */
   static int rCount = 0;

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
      animals = new Sheep.Animal [20];
      rCount = 0;
      for (int i=0; i < animals.length; i++) {
         if (Math.random() < 0.5) {
            animals[i] = Sheep.Animal.goat;
            rCount++;
         } else {
            animals[i] = Sheep.Animal.sheep;
         }
      }

      reorder(animals);

      for (Animal animal : animals) {
         System.out.println(animal);
      }


   }
   
   public static void reorder (Animal[] animals) {

      Arrays.sort(animals, Collections.reverseOrder());
   }
}

